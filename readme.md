# www.supagarn.ac.th

## Components

	Web Server (Secure) --> Image Proxy Server (image_proxy)

## Setup

1. Run **the image proxy server** by executing `cd image_proxy && python server.py`.
2. Run **the web server** by executing `cd secure && python server.py` or `cd secure && python server_prod.py`.
