# -*- coding: utf-8 -*-

from tori.application import Application

application = Application('config/prod.xml')
application.start()
