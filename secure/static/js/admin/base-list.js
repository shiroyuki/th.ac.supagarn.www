function Page() {
    this.context        = $('.container');
    this.modelContainer = this.context.find('.models');

    this.context.on('click', '.trigger.delete', $.proxy(this.onDeleteTriggerClick, this));
    this.modelContainer.on('click', '.marker', $.proxy(this.onMarkerClick, this));

    this.resetSelectionCounter();
};

$.extend(Page.prototype, {
    onMarkerClick: function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(e.currentTarget).toggleClass('checked');

        this.context.attr('data-selection-count', this.modelContainer.find('.marker.checked').length);
    },

    onDeleteTriggerClick: function (e) {
        var targets = [],
            $checkedMarkers = this.modelContainer.find('.marker.checked')
        ;

        if ($checkedMarkers.length === 0) {
            return;
        }

        $checkedMarkers.each(function (k) {
            var $marker = $(this),
                $course = $marker.parent()
            ;

            targets.push($course.attr('data-id'));
        });

        request(
            '/admin/courses/',
            'delete',
            {
                ids: targets
            },
            function (r) {
                $checkedMarkers.parent().remove();
                this.resetSelectionCounter();
            }
        );
    },

    resetSelectionCounter: function () {
        this.context.attr('data-selection-count', this.modelContainer.find('.marker.checked').length);
    }
})

$(document).ready(function () {
    page = new Page();
});
