"use strict";

var page;

function EditorPage (model, updateUrl) {
    this.updateUrl = updateUrl;
    this.model     = model;

    this.form    = document.querySelector('form.editor');
    this.preview = document.querySelector('article.preview');
    this.blickr  = BlickrController.mount(this.form.querySelector('.content'));

    this.inputName     = this.form.querySelector('input[name="name"]');
    this.inputPath     = this.form.querySelector('input[name="path"]');
    this.saveButton    = this.form.querySelector('button[type="submit"]');
    this.previewButton = this.form.querySelector('button.preview');

    this.previewDoneButton = this.preview.querySelector('button.preview-done');

    this.form.addEventListener('submit',             $.proxy(this.onFormSubmit, this));
    this.inputName.addEventListener('input',         $.proxy(this.onInputNameUpdated, this));
    this.inputPath.addEventListener('input',         $.proxy(this.onInputPathUpdated, this));
    this.saveButton.addEventListener('click',        $.proxy(this.onSaveButtonClick, this));
    this.previewButton.addEventListener('click',     $.proxy(this.onPreviewButtonClick, this));
    this.previewDoneButton.addEventListener('click', $.proxy(this.onPreviewDone, this));

    if (model) {
        this.restore(this.model);

        this.form.querySelector('button.publish').addEventListener('click',   $.proxy(this.onPublishButtonClick, this));
        this.form.querySelector('button.unpublish').addEventListener('click', $.proxy(this.onUnpublishButtonClick, this));
    } else {
        this.form.querySelector('button.publish').remove();
        this.form.querySelector('button.unpublish').remove();
    }
}

$.extend(EditorPage.prototype, {
    process: function () {
        var k,
            input,
            handlers,
            inputs = this.form.querySelectorAll('.editable'),
            data   = {
                id:          modelId,
                description: this.blickr.val(),
            }
        ;

        for (k in inputs) {
            input = inputs[k];

            if (typeof input !== 'object') {
                continue;
            }

            data[input.getAttribute('name')] = input.value;
        }

        //console.log(data);

        handlers = {
            400: function (e) { alert('Please make sure that nothing is blank.'); },
            403: function (e) { alert(e.responseJSON.detail); },
        };

        request(this.updateUrl, 'post', data, $.proxy(this.onResponse200, this), handlers);
    },

    restore: function (data) {
        var defaultPath = sanitizePathBlock(data.name);

        this.inputName.value = data.name;
        this.inputPath.value = data.path;

        this.blickr.restore(data.description);

        if (defaultPath !== data.path) {
            this.inputPath.classList.remove('auto');
        }
    },

    toggleEnableFlag: function (doEnable) {
        var data = {
                id:      modelId,
                enabled: doEnable,
            },
            self = this
        ;

        request(
            this.updateUrl,
            'post',
            data,
            function (e) {
                self.form.setAttribute('data-enabled', doEnable ? 'true' : 'false');
            }
        );
    },

    onResponse200: function (response) {
        if (this.model !== null) {
            return;
        }

        window.location = response.detail.url;
    },

    onPreviewDone: function (e) {
        e.preventDefault();

        this.form.classList.remove('in-preview');
    },

    onPreviewReady: function (response) {
        this.form.classList.add('in-preview');

        this.preview.querySelector('.content').innerHTML = response.detail;
    },

    onPreviewButtonClick: function (e) {
        rpc('web.blickr', 'render', {blickr_blocks: this.blickr.val()}, $.proxy(this.onPreviewReady, this));
    },

    onPublishButtonClick: function (e) {
        e.preventDefault();

        this.toggleEnableFlag(true);
    },

    onUnpublishButtonClick: function (e) {
        e.preventDefault();

        this.toggleEnableFlag(false);
    },

    onSaveButtonClick: function (e) {
        e.preventDefault();

        this.process();
    },

    onInputNameUpdated: function (e) {
        if (!this.inputPath.classList.contains('auto')) {
            return;
        }

        this.inputPath.value = sanitizePathBlock(this.inputName.value);
    },

    onInputPathUpdated: function (e) {
        this.inputPath.classList.remove('auto');
    },

    onFormSubmit: function (e) {
        e.preventDefault();

        this.process();
    }
})

window.addEventListener('load', function () {
    page = new EditorPage(model, updateUrl);
});

function mockData() {
    return {
        name: 'ทฤษฎีดนตรีขั้นพื้นฐาน (Basic Theory)',
        path: 'basic-theory',
        description: [
            {
                type: 'table',
                value: [
                    ['สอนโดย', 'อาจารย์ประทักษ์ ใฝ่ศุภการ']
                ]
            },
            {
                type: 'section',
                value: [
                    '* การเรียบเรียงขั้นพื้นฐาน (Arranging / Composing)',
                    '* หลักการประสานเสียง (Traditional Harmony)',
                    '* หลักการประสานเสียงแบบพ็อพ / แจ๊ส (Pop / Jazz Harmony)',
                    '* การฝึกโสต / ร้อง (Ear Training / Sight Singing)',
                    '',
                    '*Call us for more information*'
                ].join('\n')
            }
        ]
    };
}

function restoreMockData() {
    page.restore(mockData());
}