// Same implementation on the backend.
function validatePathBlock(content) {
    return (!content.test(/[\@\`\~\|\s_\/\\\;\:\+\*\<\>\?\{\}\[\]\=\(\)\"\'\$\#\%\&\~\!\^\.\,]/g));
}

// Same implementation on the backend.
function sanitizePathBlock(content) {
    content = content.replace(/[\@\`\~\|\s_\/\\\;\:\+\*\<\>\?\{\}\[\]\=\(\)\"\'\$\#\%\&\~\!\^\.\,]/g, '-');
    content = content.replace(/^-+/, '');
    content = content.replace(/-+$/, '');
    content = content.replace(/-+/g, '-');

    return content.toLocaleLowerCase();
}

function request(url, method, data, successHandler, statusCodeToHandlerMap) {
    var options = {
        method:     method,
        data:       data,
        success:    successHandler,
        statusCode: statusCodeToHandlerMap,
        complete:   function () {
            NProgress.done();
        }
    };

    NProgress.start();

    if (method != 'get') {
        options.contentType = 'application/json';
        options.data        = JSON.stringify(data);
    }

    $.ajax(url, options);
}

function rpc(component, method, params, successHandler, statusCodeToHandlerMap) {
    var data = {
        component: component,
        method:    method,
        params:    params
    }

    request(rpcUrl, 'post', data, successHandler, statusCodeToHandlerMap);
}

function useCacheImageProxy(selector, options) {
    $(selector).each(function () {
        var $image      = $(this),
            originalUrl = $image.attr('data-src') || $image.attr('src'),
            cachedUrl   = null
        ;

        if ($image.attr('data-original')) {
            return;
        }

        if (originalUrl.match(/\/p-img\//)) {
            return;
        }

        cachedUrl = getCacheImageUrl(originalUrl, options);

        $image.attr('data-original', originalUrl);
        $image.attr('src',           cachedUrl  );

        //console.log('Switched over to cache proxy:', originalUrl, cachedUrl);
    });
}

function getCacheImageUrl(url, options) {
    var imageProxyHS = window.location.hostname + ':9500', // @todo replace by the app config
        pathBlocks
    ;

    options = options || {};

    options.width  = options.width  || 'auto';
    options.height = options.height || 'auto';

    pathBlocks = [options.width, options.height, btoa(url)]

    return 'http://' + imageProxyHS + '/' + pathBlocks.join('/');
}