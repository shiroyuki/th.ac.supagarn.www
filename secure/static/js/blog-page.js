$(document).ready(function () {
    var $article         = $('.container article'),
        $photoContainers = $article.find('.photo-container.multiple')
    ;

    $photoContainers.find('img').each(function () {
        useCacheImageProxy($(this), {height: 120});
    });
});