function ImageViewer($context) {
    this.context  = $context;
    this.$image   = this.context.find('.content > img');
    this.$caption = this.context.find('.caption > span');

    this.context.on('click', '.close', $.proxy(this.close, this));
}

$.extend(ImageViewer.prototype, {
    open: function (url, caption) {
        this.context.removeClass('disabled');

        this.$image.attr('src', url);
        this.$image.attr('title', caption);
        this.$caption.html(caption);
    },

    close: function () {
        this.context.addClass('disabled');
    }
});

$(document).ready(function () {
    var $article         = $('.container article'),
        $photoContainers = $article.find('p > img').parent(),
        $imageViewer     = $('.image-viewer'),
        imageViewer
    ;

    imageViewer = new ImageViewer($imageViewer);

    $photoContainers.each(function () {
        var $container = $(this),
            $images    = $container.children('img')
        ;

        if ($images.length < 2) {
            return;
        }

        $container.addClass('photo-container');

        if ($images.length > 2) {
            $container.addClass('multiple');
        }
    });

    $article.on('click', '.photo-container.multiple img', function () {
        var url     = this.getAttribute('data-original') || this.getAttribute('data-src') || this.getAttribute('src'),
            caption = this.getAttribute('alt') || this.getAttribute('title')
        ;

        imageViewer.open(url, caption);
    });
});