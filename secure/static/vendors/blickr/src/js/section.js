function BlickrSection() {
    // ...
}

extendComponent(BlickrSection, {
    type: 'section',

    classes: [
        'element',
        'section',
    ],

    load: function (value) {
        this._input.val(value);
    },

    val: function () {
        return {
            type:  this.type,
            value: $.trim(this._input.val())
        };
    },

    ready: function () {
        this._input = this.context.find('textarea');
    },

    template: function () {
        return [
            '<div>',
                '<textarea placeholder="Start writing here!"></textarea>',
            '</div>',
        ].join('');
    }
});
