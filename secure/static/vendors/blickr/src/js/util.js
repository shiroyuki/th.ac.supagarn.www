__globalSelectors = {};

function extendComponent(Class, extension) {
    $.extend(
        Class.prototype,
        {
            $: __globalSelectors,
            renderTemplate: renderTemplate,
            is: function (type) {
                return this.context.hasClass(type);
            },
            on: function (type, handler) {
                var eventType = 'blickr.' + type;

                this.context[0].addEventListener(eventType, handler);

                return this;
            },
            fire: function (type, detail) {
                var eventType        = 'blickr.' + type,
                    dispatchingEvent = new CustomEvent(eventType, { detail: detail })
                ;

                this.context[0].dispatchEvent(dispatchingEvent);

                return this;
            },
            render: function (context) {
                return this.renderTemplate(this.template(), context);
            },
            mount: function (target, method) {
                method = method || 'appendTo';

                if (!this.$.document) {
                    this.$.document = $(document);
                }

                if (!this.$.body) {
                    this.$.body = $('body');
                }

                if (this.context) {
                    return this.context;
                }

                this.context = $(this.render({id: this.guid}));

                this.context.addClass(this.classes.join(' '));
                this.context[method](target);

                // Add the reference to this class.
                this.context[0].controller = this;

                if (this.ready) {
                    this.on('ready', $.proxy(this.ready, this));
                }

                this.fire('ready', null);
            }
        },
        extension
    );
}

function renderTemplate(content, context) {
    var r, k, v, o, p;

    o = String(content);

    for (k in context) {
        p = '\\{\\{\\s*' + k + '\\s*\\}\\}';
        r = new RegExp(p, 'g');
        v = context[k];
        o = o.replace(r, v);
    }

    return o;
}

// Controller (Singleton)
function __BlickrController() {
    // ...
}

$.extend(__BlickrController.prototype, {
    mount: function (target) {
        var container = new BlickrContainer();

        container.mount(target);

        return container;
    }
});

BlickrController = new __BlickrController();