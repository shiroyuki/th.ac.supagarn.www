function BlickrTable() {
    // ...
}

extendComponent(BlickrTable, {
    _subTemplateInput: [
        '<label title="{{ label }}">',
            '<span>{{ label }}</span>',
            '<input type="{{ type }}" name="{{ name }}" value="{{ initialValue }}" placeholder="{{ placeHolder }}" pattern="{{ pattern }}" required>',
        '</label>',
    ].join(''),

    type: 'table',

    classes: [
        'element',
        'table',
    ],

    load: function (value) {
        var rowCount    = value.length,
            columnCount = rowCount > 0 ? value[0].length : 0
        ;

        this.drawTable(rowCount, columnCount);

        this._table.find('tr').each(function (i) {
            var $row = $(this);

            $row.find('td').each(function (j) {
                var $input = $(this.querySelector('textarea'))

                $input.val(value[i][j]);
            });
        });
    },

    val: function () {
        var data = [];

        this._table.find('tr').each(function (i) {
            var $row = $(this),
                row  = []
            ;

            $row.find('td').each(function (j) {
                row.push(this.querySelector('textarea').value);
            });

            data.push(row);
        });

        return {
            type:  this.type,
            value: data
        };
    },

    drawTable: function (requestedRowCount, requestedColumnCount) {
        var currentRowCount      = this._table.find('tr').length,
            currentColumnCount   = this._table.find('tr:first').find('td').length,
            maxRowCount          = Math.max(requestedRowCount, currentRowCount),
            maxColumnCount       = Math.max(requestedColumnCount, currentColumnCount),
            i,
            j
        ;

        for (i = 0; i < maxRowCount; i++) {
            var row, rowJustCreated = false;

            if (i >= currentRowCount) {
                this._table.append('<tr></tr>');
                rowJustCreated = true;
            }

            row = this._table.find('tr').eq(i);

            if (i >= requestedRowCount) {
                row.remove();

                continue;
            }

            for (j = 0; j < maxColumnCount; j++) {
                var column;

                if (j >= currentColumnCount || rowJustCreated) {
                    row.append('<td><div><textarea></textarea></div></td>');
                }

                column = row.find('td').eq(j);

                if (j >= requestedColumnCount) {
                    column.remove();

                    continue;
                }
            }
        }

        this._form.find('input[name=row]').val(requestedRowCount);
        this._form.find('input[name=column]').val(requestedColumnCount);
    },

    ready: function () {
        this._table = this.context.find('table');
        this._form  = this.context.find('form.properties');

        this.drawTable(2, 2);

        this._form.on('change', 'input', $.proxy(this.onDimentionChanged, this));
    },

    onDimentionChanged: function (e) {
        var inputs = this._form.find('input[type=number]'),
            requestedColumnCount = parseInt(inputs.filter('[name=column]').val(), 10),
            requestedRowCount    = parseInt(inputs.filter('[name=row]').val(), 10)
        ;

        this.drawTable(requestedRowCount, requestedColumnCount);
    },

    template: function () {
        return [
            '<div>',
                '<form class="properties">',
                    this._templateInput({name: 'row',    label: 'Row',    type: 'number', initialValue: 2, pattern: '\\d+'}),
                    this._templateInput({name: 'column', label: 'Column', type: 'number', initialValue: 2, pattern: '\\d+'}),
                '</form>',
                '<div class="viewport">',
                    '<table></table>',
                '</div>',
            '</div>',
        ].join('');
    },

    _templateInput: function (option) {
        return this.renderTemplate(
            this._subTemplateInput,
            {
                name:         option.name,
                label:        option.label,
                type:         option.type         || 'text',
                initialValue: option.initialValue || '',
                placeHolder:  option.placeHolder  || '',
                pattern:      option.pattern      || '',
            }
        );
    }
});
