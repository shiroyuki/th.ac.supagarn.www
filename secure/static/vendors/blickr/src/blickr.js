__globalSelectors = {};

function extendComponent(Class, extension) {
    $.extend(
        Class.prototype,
        {
            $: __globalSelectors,
            renderTemplate: renderTemplate,
            is: function (type) {
                return this.context.hasClass(type);
            },
            on: function (type, handler) {
                var eventType = 'blickr.' + type;

                this.context[0].addEventListener(eventType, handler);

                return this;
            },
            fire: function (type, detail) {
                var eventType        = 'blickr.' + type,
                    dispatchingEvent = new CustomEvent(eventType, { detail: detail })
                ;

                this.context[0].dispatchEvent(dispatchingEvent);

                return this;
            },
            render: function (context) {
                return this.renderTemplate(this.template(), context);
            },
            mount: function (target, method) {
                method = method || 'appendTo';

                if (!this.$.document) {
                    this.$.document = $(document);
                }

                if (!this.$.body) {
                    this.$.body = $('body');
                }

                if (this.context) {
                    return this.context;
                }

                this.context = $(this.render({id: this.guid}));

                this.context.addClass(this.classes.join(' '));
                this.context[method](target);

                // Add the reference to this class.
                this.context[0].controller = this;

                if (this.ready) {
                    this.on('ready', $.proxy(this.ready, this));
                }

                this.fire('ready', null);
            }
        },
        extension
    );
}

function renderTemplate(content, context) {
    var r, k, v, o, p;

    o = String(content);

    for (k in context) {
        p = '\\{\\{\\s*' + k + '\\s*\\}\\}';
        r = new RegExp(p, 'g');
        v = context[k];
        o = o.replace(r, v);
    }

    return o;
}

// Controller (Singleton)
function __BlickrController() {
    // ...
}

$.extend(__BlickrController.prototype, {
    mount: function (target) {
        var container = new BlickrContainer();

        container.mount(target);

        return container;
    }
});

BlickrController = new __BlickrController();function BlickrInserter() {
    // ...
}

extendComponent(BlickrInserter, {
    classes: [
        'tool',
        'inserter'
    ],

    template: function () {
        return [
            '<div>',
                '<a class="toggler"></a>',
                '<a class="new-element" data-type="section">Section</a>',
                //'<a class="new-element" data-type="image">Image</a>',
                '<a class="new-element" data-type="table">Table</a>',
            '</div>'
        ].join('');
    },

    activate: function () {
        this.context.addClass('active');
    },

    deactivate: function () {
        this.context.removeClass('active');
    },

    ready: function () {
        this.$.document.on('click', $.proxy(this.onBlur, this));
        this.context.find('.toggler').on('click', $.proxy(this.onToggle, this));
        this.context.find('.new-element').on('click', $.proxy(this.onClickNewElement, this));
    },

    onToggle: function (e) {
        e.stopPropagation();

        this.context.toggleClass('active');

        this.fire(
            'activation.toggle',
            {
                active:    this.context.hasClass('active'),
                reference: this
            }
        );
    },

    onBlur: function (e) {
        this.context.removeClass('active');
    },

    onClickNewElement: function (e) {
        var type = e.currentTarget.getAttribute('data-type');

        this.fire('insert', {
            type: type,
            reference: this
        });
    }
});function BlickrImage() {
    // ...
}

extendComponent(BlickrImage, {
    classes: [
        'element',
        'image'
    ],

    template: function () {
        return [
            '<div data-url="">',
                '<img src>',
                '<a class="trigger url">Set the image URL</a>',
            '</div>',
        ].join('');
    },

    // on-dragenter="{{ onDragOver }}"
    //         on-dragover="{{ onDragOver }}"
    //         on-dragend="{{ onDragEnd }}"
    //         on-dragleave="{{ onDragEnd }}"
    //         on-drop="{{ onDrop }}"

    ready: function () {
        this.context.setAttribute('data-drag', false);

        this.on('dragenter', $.proxy(this.onDragOver, this))
            .on('dragover',  $.proxy(this.onDragOver, this))
            .on('dragend',   $.proxy(this.onDragEnd,  this))
            .on('dragleave', $.proxy(this.onDragEnd,  this))
            .on('drop',      $.proxy(this.onDrop,     this))
        ;
    },

    onDragOver: function (e) {
        e.preventDefault();

        this.context.setAttribute('data-drag', true);
    },

    onDragEnd: function (e) {
        e.preventDefault();

        this.context.setAttribute('data-drag', false);
    },

    onDrop: function (e) {
        var files = e.dataTransfer.files;

        e.preventDefault();

        this.context.setAttribute('data-drag', false);

        // Handle uploading
        // @see http://html5doctor.com/drag-and-drop-to-server/

        this.fire('upload.image', files);

        return false;
    }
});function BlickrSection() {
    // ...
}

extendComponent(BlickrSection, {
    type: 'section',

    classes: [
        'element',
        'section',
    ],

    load: function (value) {
        this._input.val(value);
    },

    val: function () {
        return {
            type:  this.type,
            value: $.trim(this._input.val())
        };
    },

    ready: function () {
        this._input = this.context.find('textarea');
    },

    template: function () {
        return [
            '<div>',
                '<textarea placeholder="Start writing here!"></textarea>',
            '</div>',
        ].join('');
    }
});
function BlickrTable() {
    // ...
}

extendComponent(BlickrTable, {
    _subTemplateInput: [
        '<label title="{{ label }}">',
            '<span>{{ label }}</span>',
            '<input type="{{ type }}" name="{{ name }}" value="{{ initialValue }}" placeholder="{{ placeHolder }}" pattern="{{ pattern }}" required>',
        '</label>',
    ].join(''),

    type: 'table',

    classes: [
        'element',
        'table',
    ],

    load: function (value) {
        var rowCount    = value.length,
            columnCount = rowCount > 0 ? value[0].length : 0
        ;

        this.drawTable(rowCount, columnCount);

        this._table.find('tr').each(function (i) {
            var $row = $(this);

            $row.find('td').each(function (j) {
                var $input = $(this.querySelector('textarea'))

                $input.val(value[i][j]);
            });
        });
    },

    val: function () {
        var data = [];

        this._table.find('tr').each(function (i) {
            var $row = $(this),
                row  = []
            ;

            $row.find('td').each(function (j) {
                row.push(this.querySelector('textarea').value);
            });

            data.push(row);
        });

        return {
            type:  this.type,
            value: data
        };
    },

    drawTable: function (requestedRowCount, requestedColumnCount) {
        var currentRowCount      = this._table.find('tr').length,
            currentColumnCount   = this._table.find('tr:first').find('td').length,
            maxRowCount          = Math.max(requestedRowCount, currentRowCount),
            maxColumnCount       = Math.max(requestedColumnCount, currentColumnCount),
            i,
            j
        ;

        for (i = 0; i < maxRowCount; i++) {
            var row, rowJustCreated = false;

            if (i >= currentRowCount) {
                this._table.append('<tr></tr>');
                rowJustCreated = true;
            }

            row = this._table.find('tr').eq(i);

            if (i >= requestedRowCount) {
                row.remove();

                continue;
            }

            for (j = 0; j < maxColumnCount; j++) {
                var column;

                if (j >= currentColumnCount || rowJustCreated) {
                    row.append('<td><div><textarea></textarea></div></td>');
                }

                column = row.find('td').eq(j);

                if (j >= requestedColumnCount) {
                    column.remove();

                    continue;
                }
            }
        }

        this._form.find('input[name=row]').val(requestedRowCount);
        this._form.find('input[name=column]').val(requestedColumnCount);
    },

    ready: function () {
        this._table = this.context.find('table');
        this._form  = this.context.find('form.properties');

        this.drawTable(2, 2);

        this._form.on('change', 'input', $.proxy(this.onDimentionChanged, this));
    },

    onDimentionChanged: function (e) {
        var inputs = this._form.find('input[type=number]'),
            requestedColumnCount = parseInt(inputs.filter('[name=column]').val(), 10),
            requestedRowCount    = parseInt(inputs.filter('[name=row]').val(), 10)
        ;

        this.drawTable(requestedRowCount, requestedColumnCount);
    },

    template: function () {
        return [
            '<div>',
                '<form class="properties">',
                    this._templateInput({name: 'row',    label: 'Row',    type: 'number', initialValue: 2, pattern: '\\d+'}),
                    this._templateInput({name: 'column', label: 'Column', type: 'number', initialValue: 2, pattern: '\\d+'}),
                '</form>',
                '<div class="viewport">',
                    '<table></table>',
                '</div>',
            '</div>',
        ].join('');
    },

    _templateInput: function (option) {
        return this.renderTemplate(
            this._subTemplateInput,
            {
                name:         option.name,
                label:        option.label,
                type:         option.type         || 'text',
                initialValue: option.initialValue || '',
                placeHolder:  option.placeHolder  || '',
                pattern:      option.pattern      || '',
            }
        );
    }
});
// Container
function BlickrContainer() {
    this.guid    = this._lastCUID++;
    this.context = null;
}

extendComponent(BlickrContainer, {
    _lastCUID: 0,

    _componentMap: {
        section:  BlickrSection,
        inserter: BlickrInserter,
        //image:    BlickrImage,
        table:    BlickrTable,
    },

    _subTemplateAddons: '<div class="addons"><a class="addon trigger element-remover" title="Remove this element."></a></div>',

    classes: [
        'blickr',
        'core',
    ],

    restore: function (serializedElements) {
        var se, // serialized element
            e,  // blickr element
            i,
            tas, // textareas
            l = serializedElements.length
        ;

        this.context.empty();

        for (i = 0; i < l; i++) {
            se = serializedElements[i];
            e  = this.addElement(se.type);

            e.load(se.value);
        }

        this.context.find('textarea').trigger('input');
    },

    val: function () {
        var sequence = [];

        this.context.children('.element').each(function (i) {
            sequence.push(this.controller.val());
        });

        return sequence;
    },

    template: function () {
        return '<div class="blickr" data-guid="{{ id }}"></div>';
    },

    addElement: function (type, reference) {
        var element, inserter;

        element  = this.add(type, reference);
        inserter = this.add('inserter', element);

        // Bind events for a new inserter.
        inserter.on('insert',            $.proxy(this.onInserterInsert, this));
        inserter.on('activation.toggle', $.proxy(this.onInserterActivationToggle, this));

        // Inject the element remover.
        element.context.children().eq(0).before(this._subTemplateAddons);

        this.fire('structure.change', null);

        return element;
    },

    add: function (type, reference) {
        var component, inserter;

        if (!this._componentMap[type]) {
            throw 'shiroyuki.blickr.UnknownComponent';
        }

        component = new (this._componentMap[type])();

        if (reference) {
            component.mount(reference.context, 'insertAfter');

            return component;
        }

        component.mount(this.context, 'appendTo');

        return component;
    },

    ready: function () {
        // DOM Event: Action: Remove the entire element (sub-component/ancestor container).
        this.context.on(
            'click',
            '.trigger.element-remover',
            $.proxy(this.onClickRemoveElement, this)
        );

        // DOM Event: Action: Automatically resize all textarea's like Facebook input.
        this.context.on(
            'input',
            'textarea',
            $.proxy(this.onChangeTextareaResize, this)
        );

        // DOM Event: Action: Disable highlighting.
        this.context.on(
            'click',
            '.trigger, .toggler',
            this.onClickDisableAccidentalHighlighting
        );

        // DOM Event: Action: Disable highlighting.
        this.context.on(
            'mousedown',
            '.trigger, .toggler',
            this.onClickDisableAccidentalHighlighting
        );

        // Local Event: Action: Update the element count.
        this.on(
            'structure.change',
            $.proxy(this.onStructuralChange, this)
        );

        // Add the default element (section).
        this.addElement('section');
    },

    onStructuralChange: function (e) {
        this.context.attr('data-element-count', this.context.children('.element').length);
    },

    onInserterInsert: function (e) {
        this.addElement(e.detail.type, e.detail.reference);
    },

    onInserterActivationToggle: function (e) {
        this.context.find('.tool.inserter').each(function (index) {
            if (e.detail.reference === this.controller) {
                return;
            }

            this.controller.deactivate();
        });
    },

    onChangeTextareaResize: function (e) {
        var element = e.currentTarget,
            context = $(element),
            previousHeight = context.innerHeight()
        ;

        context
            .css({'height':'auto'})
            .height(element.scrollHeight);
    },

    onClickDisableAccidentalHighlighting: function (e) {
        e.preventDefault();
    },

    onClickRemoveElement: function (e) {
        var target           = $(e.currentTarget).closest('.element'),
            collateralDamage = target.next()
        ;

        e.preventDefault();

        collateralDamage.remove();
        target.remove();

        this.fire('structure.change', null);
    }
});

