from web.common import BaseController

class EndPoint(BaseController):
    def post(self):
        component_id     = self.field('component')
        requested_method = self.field('method')
        parameters       = self.field('params')

        service = self.component(component_id)

        self.respond_json(
            200,
            service.__getattribute__(requested_method)(**parameters)
        )
