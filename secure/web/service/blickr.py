import base64
import codecs
import re

import markdown
from tori.centre import settings as AppSettings

from web.common import RpcInterface

class Blickr(RpcInterface):
    re_src = re.compile('\!\[(?P<alt>[^\]]*)\]\((?P<src>[^\)]+)\)', re.MULTILINE)

    def __init__(self, image_proxy_enabled=True):
        self._image_proxy_server  = AppSettings['image_proxy']['server']
        self._image_proxy_default = AppSettings['image_proxy']['default']
        self._image_proxy_enabled = image_proxy_enabled

    def render(self, blickr_blocks):
        script = self._convert_blickr_to_markdown(blickr_blocks)

        if self._image_proxy_enabled:
            matches = self.re_src.search(script)

            while matches:
                attributes = matches.groupdict()
                cache_url  = '{}/{}/{}/{}'.format(
                    self._image_proxy_server,
                    self._image_proxy_default['width'],
                    self._image_proxy_default['height'],
                    base64.b64encode(attributes['src'])
                )

                #print(attributes)

                script = self.re_src.sub(
                    u'<img title="{}" src="{}" data-src="{}"/>'.format(attributes['alt'], cache_url, attributes['src']),
                    script,
                    count=1
                )

                # Check for more
                matches = self.re_src.search(script)

        output = markdown.markdown(script)

        #print(script)
        #print(output)

        return output

    def _convert_blickr_to_markdown(self, blickr_blocks):
        blocks = []

        for block in blickr_blocks:
            t = block['type']
            v = block['value']

            if t == 'section':
                blocks.append(v)

                continue

            elif t == 'table':
                content = ['<table>']

                for r in v:
                    content.append('<tr>')

                    for c in r:
                        content.append(u'<td>{}</td>'.format(c))

                    content.append('</tr>')

                content.append('</table>')

                blocks.append(''.join(content))

                continue

            raise RuntimeError('Unsupported Blickr Element "{}"'.format(t))

        return '\n\n'.join(blocks)