class MemoryCollection(object):
    def __init__(self):
        self._data = {}

    def get(self, key):
        return self._data[key] if self.has(key) else None

    def set(self, key, value):
        self._data[key] = value

    def has(self, key):
        return key in self._data

    def drop(self, key):
        if not self.has(key):
            return

        del self._data[key]