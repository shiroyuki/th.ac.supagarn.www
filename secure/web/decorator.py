from tornado.web import HTTPError

# Decorator for controller methods
def restricted_access(permissions):
    if not isinstance(permissions, list):
        raise ValueError('The list of permissions is required to restrict the access.')

    def decorator(reference):
        def new_method(self, *args, **kwargs):
            if not self.current_user:
                return self.raise_error(401)

            user = self.current_user

            for permission in permissions:
                if permission not in user.permissions:
                    return self.raise_error(403)

            # Execute the reference method.
            return reference(self, *args, **kwargs)
        # enddef

        return new_method
    # enddef

    return decorator
