""" Model Validation

    This is prototype for Passerine 2.3.
"""
from imagination.proxy import Proxy

class UndefinedConstraint(ValueError):
    pass

class InvalidConstraint(ValueError):
    pass

class PropertyDataTrasformerInterface(object):
    def transform(self, value):
        raise NotImplemented()

class ConstraintInterface(object):
    def check(self, value):
        raise NotImplemented()

    def respond(self, code, reason=None):
        return {
            'code': code,
            'reason': reason,
        }

class NotEmptyConstraint(ConstraintInterface):
    def check(self, value):
        if value:
            return None

        return self.respond('core.constraint.not_empty.empty_given')

class TypeConstraint(ConstraintInterface):
    def __init__(self, expected_type):
        if not isinstance(expected_type, type):
            return ValueError('The expected type must be a type (aka class).')

        self._expected_type = expected_type

    def check(self, value):
        if isinstance(value, self._expected_type):
            return None

        return self.respond('core.constraint.type.mismatch', type(value).__name__)

class StringConstraint(ConstraintInterface):
    def check(self, value):
        if isinstance(value, str):
            return None

        try:
            if isinstance(value, unicode):
                return None
        except NameError as e:
            pass # NOP; implemented for Python 2

        return self.respond('core.constraint.type.not_string', type(value).__name__)

class PropertyMetadata(object):
    def __init__(self, constraints, transformers):
        self._constraints  = constraints
        self._transformers = transformers

    @property
    def constraints(self):
        for c in self._constraints:
            yield c

def define(name, constraints=[], transformers=[]):
    config_property_name = '__p3_properties__' # duplicated

    def inner_decorator(cls):
        if config_property_name not in dir(cls):
            cls.__p3_properties__ = {}

        properties = cls.__p3_properties__

        if name not in properties:
            properties[name] = PropertyMetadata(constraints, transformers)

        return cls

    return inner_decorator

def extract_metadata(model):
    config_property_name = '__p3_properties__' # duplicated
    cls = type(model)

    if config_property_name not in dir(cls):
        return {}

    return cls.__p3_properties__

class Validator(object):
    def __init__(self, contraints={}):
        self._registered_contraints = contraints

    def validate(self, model, ignore_none_type):
        metadata = extract_metadata(model)
        errors   = []

        for property_name in metadata:
            property_metadata = metadata[property_name]

            for constraint_name in property_metadata.constraints:
                if constraint_name not in self._registered_contraints:
                    raise UndefinedConstraint()

                constraint = self._registered_contraints[constraint_name]

                if isinstance(constraint, Proxy):
                    constraint = constraint.load()

                if not isinstance(constraint, ConstraintInterface):
                    raise InvalidConstraint('The type of constraint "{}", {}, is not usable.'.format(constraint_name, type(constraint)))

                property_value = model.__getattribute__(property_name)

                if ignore_none_type and property_value == None:
                    continue

                violation = constraint.check(property_value)

                # Upon the detection of violation, the validation for the
                # current property will be skipped.
                if violation:
                    errors.append({
                        'property':  property_name,
                        'violation': violation,
                    })

                    break
            # endfor

        # endfor

        return errors

##### The following is specifically for this project. #####
class BlickrConstraint(ConstraintInterface):
    def check(self, value):
        for block in value:
            if not isinstance(block, dict):
                return 'web.constraint.blickr.invalid_block_structure'

            if 'type' not in block:
                return 'web.constraint.blickr.undefined_block_type'

            if 'value' not in block:
                return 'web.constraint.blickr.undefined_block_value'

        return None # no violation
