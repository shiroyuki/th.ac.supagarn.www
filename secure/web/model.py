import time
from passerine.db.entity import entity
from passerine.db.mapper import link, AssociationType as t, CascadingType as c
from web.validation      import define

@link('credential', 'web.model.Credential', association=t.MANY_TO_ONE, cascading=[c.PERSIST, c.DELETE, c.REFRESH]
)
@entity('security.credential_original')
class CredentialOriginal(object):
    def __init__(self, credential, data, code, created=None):
        self.credential = credential
        self.data       = data
        self.code       = code
        self.created    = created or time.time()

@link('user', 'web.model.User', association=t.MANY_TO_ONE, cascading=[c.PERSIST, c.DELETE, c.REFRESH])
@entity('security.credential')
class Credential(object):
    def __init__(self, user, provider, identifier, password=None, secret=None, enabled=True, created=None):
        self.user       = user
        self.provider   = provider
        self.identifier = identifier
        self.password   = password
        self.secret     = secret
        self.enabled    = enabled
        self.created    = created or time.time()

@entity('security.users')
class User(object):
    def __init__(self, name, avatar, alias=None, enabled=True, groups=[], permissions=[]):
        self.alias       = alias
        self.name        = name
        self.avatar      = avatar
        self.enabled    = enabled
        self.groups      = groups
        self.permissions = permissions

@define('name', ['type.str', 'not_empty'])
@define('path', ['type.str', 'not_empty'])
@define('description', ['type.list', 'blickr'])
@define('enabled', ['type.bool'])
class BasePage(object):
    def __init__(self, name, path, description=[], enabled=False, created_at=None, modified_at=None):
        self.name        = name
        self.path        = path
        self.description = description
        self.enabled     = enabled
        self.created_at  = created_at  or time.time()
        self.modified_at = modified_at or time.time()

@entity('pages')
class Page(BasePage): pass

@entity('blog.posts')
class Blog(BasePage): pass

@entity('courses')
class Course(BasePage):
    def __init__(self, name, path, description=[], enabled=False, created_at=None, modified_at=None):
        self.name        = name
        self.path        = path
        self.description = description
        self.enabled     = enabled
        self.created_at  = created_at  or time.time()
        self.modified_at = modified_at or time.time()

@link('owner', User, association=t.ONE_TO_ONE, read_only=True)
@entity('profiles')
class Profile(object):
    def __init__(self, name, path, description=[], enabled=False, active=True, owner=None, title=None, created_at=None, modified_at=None):
        self.name        = name
        self.path        = path
        self.description = description
        self.enabled     = enabled
        self.active      = active
        self.owner       = owner
        self.title       = title
        self.created_at  = created_at  or time.time()
        self.modified_at = modified_at or time.time()

class UserSession(dict):
    @property
    def user(self):
        return self['user']

    @property
    def reference(self):
        return self['reference']

    @property
    def expiry(self):
        return self['expiry']

    def set_user(self, user):
        self['user'] = user

    def set_reference(self, ref):
        self['reference'] = ref

    def set_expiry(self, expiry):
        self['expiry'] = expiry
