from web.admin.base_controllers import BaseList, BaseEditor

class UIList(BaseList):
    def _iapi_id(self):
        return 'iapi.blog'

    def _list_template_name(self):
        return 'blog/list'

class UIEditor(BaseEditor):
    def _route_id(self):
        return 'admin.blog.editor.update'

    def _cache_api_id(self):
        return 'web.cache.blog'

    def _iapi_id(self):
        return 'iapi.blog'

    def _list_template_name(self):
        return 'blog/editor'
