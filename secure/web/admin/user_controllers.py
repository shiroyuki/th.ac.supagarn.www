from tornado.web import HTTPError
from web.common import BaseController
from web.model import Course

class UserWebApi(BaseController):
    def post(self, uid=None):
        if not self.auth_user:
            raise HTTPError(401)

        if not uid:
            raise HTTPError(403, 'User creation disabled')

        user = self.component('iapi.user').get(uid)

        if not user:
            raise HTTPError(404)

        self.render('admin/course_editor.html', course = course)

    def __get_all(self):
        courses = None

        with self.component('db.default').session() as session:
            repository = session.repository(Course)
            courses    = repository.find()

        return courses

    def __get(self, course_id):
        course = None

        with self.component('db.default').session() as session:
            repository = session.repository(Course)
            course     = repository.get(ObjectId(course_id))

        return course