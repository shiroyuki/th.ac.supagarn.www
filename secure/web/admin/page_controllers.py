from web.admin.base_controllers import BaseList, BaseEditor

class UIList(BaseList):
    def _iapi_id(self):
        return 'iapi.page'

    def _list_template_name(self):
        return 'pages/list'

class UIEditor(BaseEditor):
    def _route_id(self):
        return 'admin.page.editor.update'

    def _cache_api_id(self):
        return 'web.cache.page'

    def _iapi_id(self):
        return 'iapi.page'

    def _list_template_name(self):
        return 'pages/editor'
