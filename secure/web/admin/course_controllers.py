from web.admin.base_controllers import BaseList, BaseEditor

class UIList(BaseList):
    def _iapi_id(self):
        return 'iapi.course'

    def _list_template_name(self):
        return 'courses/list'

class UIEditor(BaseEditor):
    def _route_id(self):
        return 'admin.course.editor.update'

    def _cache_api_id(self):
        return 'web.cache.course'

    def _iapi_id(self):
        return 'iapi.course'

    def _list_template_name(self):
        return 'courses/editor'
