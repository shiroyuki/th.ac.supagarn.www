from web.common import SESSION_KEY_USER, BaseController

class Login(BaseController):
    def get(self):
        if self.auth_user:
            return self.redirect_to('user.settings')

        return self.render('login.html')

class Logout(BaseController):
    def get(self):
        self.session.delete(SESSION_KEY_USER)
        self.redirect('/')