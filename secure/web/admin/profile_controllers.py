from web.admin.base_controllers import BaseList, BaseEditor

class UIList(BaseList):
    def _iapi_id(self):
        return 'iapi.profile'

    def _list_template_name(self):
        return 'profiles/list'

class UIEditor(BaseEditor):
    def _route_id(self):
        return 'admin.profile.editor.update'

    def _cache_api_id(self):
        return 'web.cache.profile'

    def _iapi_id(self):
        return 'iapi.profile'

    def _list_template_name(self):
        return 'profiles/editor'
