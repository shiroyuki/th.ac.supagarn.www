import re
from time            import time
from tornado.auth    import FacebookGraphMixin, AuthError
from tornado.gen     import coroutine
from web.common      import BaseController, SESSION_KEY_USER
from web.model       import *

class FacebookGraphLoginHandler(BaseController, FacebookGraphMixin):
    @coroutine
    def get(self):
        session = self._restore_session()

        if session:
            print('Has a session. Redirecting to index.')
            self.redirect('/')
        else:
            print('NOT have a session.')

            redirect_uri = self._get_redirect_url()

            print(redirect_uri)

            app_id = self.settings["facebook"]["app_id"]
            secret = self.settings["facebook"]["secret"]

            code = self.get_argument("code", None)

            if code:
                print('Detected the code.')

                try:
                    raw_user = yield self.get_authenticated_user(
                        redirect_uri  = redirect_uri,
                        client_id     = app_id,
                        client_secret = secret,
                        code          = code
                    )

                    co = self._get_credential_original(raw_user, code)

                    print('Initializing the session.')

                    self._initiate_session(co)
                    self.redirect('/')
                except RuntimeError as e:
                    print('RuntimeError: {}'.format(e))
                except AuthError as e:
                    print('AuthError!')
                    pass
            else:
                yield self.authorize_redirect(
                    redirect_uri = redirect_uri,
                    client_id    = app_id,
                    extra_params = {
                        #"scope": "offline_access"
                    }
                )

                print('NOT detected the code.')

    def _get_credential_original(self, oauth_user, code):
        current_user = self.current_user
        identifier   = oauth_user['id']

        credentials = self.component('iapi.credential')
        credential_originals = self.component('iapi.credential_original')

        c = credentials.simple_find({'identifier': identifier}, 1)

        if current_user and c and current_user.id == c.user.id:
            raise RuntimeError('No identity take-over')

        if not c:
            c = Credential(current_user, 'facebook', identifier)

            if not current_user:
                u = User(
                    oauth_user['name'],
                    oauth_user['picture']['data']['url'],
                    self._get_user_alias(oauth_user['name'])
                )

                c.user = u

        co = CredentialOriginal(c, oauth_user, code)

        credential_originals.write(co)

        return co

    def _get_user_alias(self, name):
        return re.sub('\s+', '.', name.lower().strip())

    def _restore_session(self):
        session_data = self.session.get(SESSION_KEY_USER)

        return UserSession(session_data) if session_data else None

    def _initiate_session(self, co):
        session = UserSession()
        session.set_user(str(co.credential.user.id))
        session.set_reference(str(co.credential.id))
        session.set_expiry(int(co.data['session_expires'][0]))

        self.session.set(SESSION_KEY_USER, session)

    def _get_redirect_url(self):
        return '{protocol}://{hostname}{request_path}'.format(
            protocol = self.request.protocol,
            hostname = self.request.host,
            request_path = self.component('routing_map').resolve('auth.inf.fb')
        )
