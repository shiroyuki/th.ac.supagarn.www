import time
from web.decorator import restricted_access
from web.common    import update, BaseController

class BaseList(BaseController):
    @restricted_access([])
    def get(self):
        return self.render(
            'admin/{}.html'.format(self._list_template_name()),
            models = self._iapi.all()
        )

    @restricted_access([])
    def delete(self):
        iapi = self._iapi
        keys = self.field('ids')

        for key in keys:
            iapi.delete(key)

        self.respond_json(200, {})

    @property
    def _iapi(self):
        iapi = self.component(self._iapi_id())

        if not iapi:
            raise RuntimeError(self._iapi_id())

        return iapi

    def _iapi_id(self):
        raise NotImplemented('IAPI ID not defined')

    def _list_template_name(self):
        raise NotImplemented('IAPI ID not defined')

class BaseEditor(BaseController):
    @restricted_access([])
    def get(self, key = None):
        template_path = 'admin/{}.html'.format(self._list_template_name())

        # New page
        if not key or key == 'new':
            return self.render(template_path, model = None)

        # Update page
        model = self._iapi.get(key)

        if not model:
            return self.raise_error(404)

        self.render(template_path, model = model, update_url = self._update_url(model))

    @restricted_access([])
    def post(self, key = None):
        iapi      = self._iapi
        validator = self.component('validator')

        if self._may_duplicate():
            return self.raise_error(403, 'The path has already taken.')

        model = None

        model_id = self.field('id')

        if model_id:
            model = iapi.get(model_id)

            if not model:
                return self.respond_json(404)

            update(model, self.fields())

            model.updated = time.time()

        else:
            fields = dict(self.fields())
            del fields['id'];

            model = iapi.new(**fields)

        errors = validator.validate(model, True)

        if errors:
            return self._handle_errors(errors)

        iapi.write(model)
        self.component(self._cache_api_id()).drop(model.path)

        self.respond_json(200, {
            'id':  str(model.id),
            'url': self._update_url(model)
        })

    def _update_url(self, model):
        return self.resolve_route(self._route_id(), {'key': model.id})

    def _may_duplicate(self):
        if not self.field('path'):
            return False

        potential_duplicates = self.component(self._iapi_id()).simple_find({'path': self.field('path')})

        for reference in potential_duplicates:
            if str(reference.id) == self.field('id'):
                continue

            if reference.path == self.field('path'):
                return True

        return False

    @property
    def _iapi(self):
        return self.component(self._iapi_id())

    def _handle_errors(self, errors):
        return self.raise_error(400, errors)

    def _route_id(self):
        raise NotImplemented('Route ID not defined')

    def _iapi_id(self):
        raise NotImplemented('IAPI ID not defined')

    def _cache_api_id(self):
        raise NotImplemented('IAPI ID not defined')

    def _list_template_name(self):
        raise NotImplemented('Template name not defined')
