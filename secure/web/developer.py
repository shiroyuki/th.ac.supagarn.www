from web.common import BaseController, SESSION_KEY_USER
from web.model  import UserSession

class SecurityBypasser(BaseController):
    def get(self, key=None):
        iapi = self.component('iapi.user')

        if not key:
            users = iapi.all()

            return self.render(
                'developer_security_bypasser.html',
                users = users
            )

        user    = iapi.get(key)
        session = UserSession()

        # This session has no expiry date and reference (deprecated).
        session.set_user(key)

        self.session.set(SESSION_KEY_USER, session)

        self.redirect_to('home')
