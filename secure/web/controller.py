import os
import random
from web.common  import BaseController

# Not implemented yet
class Home(BaseController):
    def get(self):
        image_names = []

        for name in os.listdir('static/p-img/homepage'):
            if '.' == name[0]:
                continue

            image_names.append(name)

            random.shuffle(image_names)

        self.render('home.html', name = 'web', image_names = image_names)

class Page(BaseController):
    def get(self, key):
        iapi   = self.component('iapi.page')
        cache  = self.component('web.cache.page')
        blickr = self.component('web.blickr')

        if cache.has(key):
            return self.render('page.html', model = cache.get(key))

        page = iapi.simple_find({'path': key, 'enabled': True}, 1)

        if not page:
            return self.raise_error(404, key)

        model = {
            'name': page.name,
            'path': page.path,
            'description': blickr.render(page.description)
        }

        cache.set(key, model)

        return self.render('page.html', model = model)

class Sitemap(BaseController):
    def get(self):
        iapi = self.component('iapi.page')

        return self.render(
            'sitemap.html',
            pages = iapi.simple_find({'enabled': True})
        )

class BaseContentController(BaseController):
    def get(self, key=None):
        iapi = self.component('iapi.{}'.format(self._service_namespace()))

        list_template = '{}/list.html'.format(self._template_namespace())
        page_template = '{}/page.html'.format(self._template_namespace())

        if not key:
            return self.render(
                list_template,
                namespace = self._service_namespace(),
                models    = iapi.simple_find({'enabled': True})
            )

        cache  = self.component('web.cache.{}'.format(self._service_namespace()))
        blickr = self.component('web.blickr')

        if cache.has(key):
            return self.render(
                page_template,
                namespace = self._service_namespace(),
                model     = cache.get(key)
            )

        entity = iapi.simple_find({'path': key, 'enabled': True}, 1)

        if not entity:
            return self.raise_error(404)

        model = {
            'name': entity.name,
            'path': entity.path,
            'description': blickr.render(entity.description),
            'created_at': entity.created_at,
            'modified_at': entity.modified_at,
        }

        cache.set(key, model)

        return self.render(
            page_template,
            namespace = self._service_namespace(),
            model     = model
        )

    def _service_namespace(self):
        raise NotImplemented()

    def _template_namespace(self):
        raise NotImplemented()

class Blog(BaseContentController):
    def _service_namespace(self):
        return 'blog'

    def _template_namespace(self):
        return 'blog'

class Course(BaseContentController):
    def _service_namespace(self):
        return 'course'

    def _template_namespace(self):
        return 'courses'

class Profile(BaseContentController):
    def _service_namespace(self):
        return 'profile'

    def _template_namespace(self):
        return 'profiles'

# Not implemented yet
class UserConfiguration(BaseController):
    def get(self):
        if not self.auth_user:
            return self._redirect_to_auth()

        self.render('settings.html')
