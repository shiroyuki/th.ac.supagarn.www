from bson.objectid                import ObjectId
from passerine.db.metadata.helper import EntityMetadataHelper
from passerine.db.exception       import EntityNotRecognized

class BaseDataAPI(object):
    def __init__(self, repo):
        self.repo = repo

    def new(self, **attributes):
        return self.repo.new(**attributes)

    def get(self, entity_id):
        entity_id = entity_id if isinstance(entity_id, ObjectId) else ObjectId(entity_id)

        return self.repo.get(entity_id)

    def write(self, entity):
        self.repo.persist(entity)
        self.repo.commit()

    def all(self):
        return self.repo.find()

    def find(self, query):
        return self.repo.find(query)

    def delete(self, entity):
        try:
            entity = entity \
                if EntityMetadataHelper.hasMetadata(type(entity)) \
                else self.get(entity)

            self.repo.delete(entity)
        except EntityNotRecognized as e:
            pass

    def simple_find(self, filter_map, limit=None):
        query = self.criteria('e')

        for k in filter_map:
            v = filter_map[k]

            query.expect('e.{k} = :{k}'.format(k = k))
            query.define(k, v)

        if limit:
            query.limit(limit)

        return self.find(query)

    def criteria(self, alias):
        return self.repo.new_criteria(alias)
