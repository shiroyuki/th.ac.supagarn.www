import codecs
import json
import re
import time
from tornado.web import HTTPError
from tori.controller import Controller
from web.model       import User, UserSession

SESSION_KEY_USER = 'user'

def update(entity, updated, ignored_properties = []):
    ignored_properties.extend(['id', '_id'])

    for k in updated:
        if k in ignored_properties:
            continue

        entity.__setattr__(k, updated[k])

class RpcInterface(object): pass

class BaseController(Controller):
    RE_MOBILE_UA = re.compile('(Android|iPad|iPhone|Mobile)')
    RE_PHONE_UA  = re.compile('(Mobile)')

    def __init__(self, *l, **kw):
        super(BaseController, self).__init__(*l, **kw)

        self._auth_user_loaded = False
        self._auth_user        = None
        self._fields_loaded    = False
        self._fields           = {}

    def status_to_route_map(self):
        return {
            401: 'login',
        }

    @property
    def auth_user(self):
        if self._auth_user_loaded:
            return self._auth_user

        session_data    = self.session.get(SESSION_KEY_USER)
        self._auth_user = UserSession(session_data) if session_data else None

        self._auth_user_loaded = True

        return self._auth_user

    @property
    def current_user(self):
        if not self.auth_user:
            return None

        return self.find_user(self.auth_user.user)

    @property
    def is_mobile(self):
        return bool(
            'User-Agent' in self.request.headers\
            and self.RE_MOBILE_UA.search(self.request.headers['User-Agent'])
        )

    @property
    def device_form_factor(self):
        if not self.is_mobile:
            return 'computer'

        if self.RE_PHONE_UA.search(self.request.headers['User-Agent']):
            return 'phone'

        return 'tablet'

    def find_user(self, pk):
        iapi = self.component('iapi.user')

        return iapi.get(pk)

    def render_template(self, template_name, **contexts):
        contexts.update({
            'auth_user':          self.auth_user,
            'find_user':          self.find_user,
            'current_user':       self.current_user,
            'static_path':        self.resolve_static_path,
            'to_dict':            self.convert_entity_to_dict,
            'to_json':            json.dumps,
            'is_mobile':          self.is_mobile,
            'device_form_factor': self.device_form_factor,
            'time_format':        self.format_unixtime,
            'time_ago':           self.convert_unixtime_to_time_elapsed,
        })

        return super(BaseController, self).render_template(template_name, **contexts)

    def convert_unixtime_to_time_elapsed(self, unixtime):
        steps    = [60, 60, 24]
        labels   = ['m', 'h', 'd']
        max_step = len(steps)

        diff = time.time() - unixtime

        step = -1

        while step < max_step and diff > steps[step + 1]:
            step += 1
            diff %= steps[step]

        if step < 0 or (step == 1 and diff < 5):
            return None

        return '{}{}'.format(int(diff), labels[step])

    def format_unixtime(self, unixtime):
        return time.strftime('%Y-%m-%d %H:%M', time.gmtime(unixtime))

    def convert_entity_to_dict(self, o):
        s = self.component('util.serializer')
        d = s.encode(o)

        d['id'] = str(d['_id'])

        del d['_id']

        return d

    def resolve_static_path(self, path):
        return self.component('routing_map').resolve('static', path = path)

    def _redirect_to_auth(self):
        self.redirect_to('auth.inf.fb')

    def _read_json(self, data):
        try:
            return json.loads(data)
        except TypeError as e:
            return json.loads(data.decode('utf-8'))

    def render_json(self, data):
        self.write(json.dumps(data))

    def respond_json(self, code, data):
        self.set_status(code)
        self.set_header('content-type', 'application/json')
        self.render_json({
            'success': code >= 200 and code < 300,
            'detail':  data
        })

    def raise_error(self, code, data = {}):
        if self.is_xhr:
            return self.respond_json(code, data)

        route_map = self.status_to_route_map()

        if code in route_map:
            return self.redirect_to(route_map[code])

        raise HTTPError(code, json.dumps(data))

    def fields(self):
        if not self._fields_loaded:
            data = self._read_json(self.request.body)
            self._fields.update(data)

        return self._fields

    def field(self, key):
        fs = self.fields()

        return fs[key] if key in fs else None
